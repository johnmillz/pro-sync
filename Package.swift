// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ProSync",
    platforms:  [
           .macOS(.v10_14),
       ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .executable(
            name: "ProSync",
            targets: ["ProSync"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "SwiftJWT", url: "https://github.com/IBM-Swift/Swift-JWT", from: "3.6.1"),
        .package(url: "https://github.com/jakeheis/SwiftCLI", from: "6.0.1"),
        .package(url: "https://github.com/mxcl/PromiseKit", from: "6.13.1"),
        .package(url: "https://github.com/onevcat/Rainbow", from: "3.1.5"),
        .package(url: "https://github.com/kishikawakatsumi/KeychainAccess", from: "4.2.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "ProSync",
            dependencies: ["SwiftJWT", "SwiftCLI", "PromiseKit", "Rainbow", "KeychainAccess"]),
        .testTarget(
            name: "ProSyncTests",
            dependencies: ["ProSync"]),
    ]
)
