//
//  AppStoreConnect.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import KeychainAccess
import PromiseKit
import Rainbow

struct AppStoreConnect: SignedGateway {
    
    enum SaveMode {
        case diff
        case sync
    }
    
    let credentialStore: CredentialLoading

    init(credentialStore: CredentialLoading) {
        self.credentialStore = credentialStore
    }

    func downloadProfiles(mode: SaveMode) throws {
        print("creating signed download request".blue)
        let queryItems = [URLQueryItem(name: "filter[profileState]", value: "ACTIVE"), URLQueryItem(name: "limit", value: "200")]
        let request = try createRequest(endpoint: "profiles", queryItems: queryItems, credentialStore: credentialStore)
        
        let client = HTTPSClient()
        print("downloading provisioning profiles".blue)
        let runLoop = CFRunLoopGetCurrent()
        firstly {
            client.makeRequest(request)
        }.compactMap { data in
            try JSONDecoder().decode(ProfileResponse.self, from: data)
        }.done { response in
            switch mode {
            case .diff:
                let newerProfiles = self.compare(profiles: response.data.compactMap{ $0.attributes })
                try self.save(profiles: newerProfiles)
            case .sync:
                try self.save(profiles: response.data.compactMap{ $0.attributes })
            }
            CFRunLoopStop(runLoop)
        }.catch { error in
            print(error.localizedDescription.red.onBlack)
        }
        CFRunLoopRun()
    }
    
    fileprivate func save(profiles: [Attribute]) throws {
        let baseURL = URL.provisioningProfilesPath()
        
        try FileManager.default.createDirectory(at: baseURL, withIntermediateDirectories: true)
        try profiles.forEach{
            let data = Data(base64Encoded: $0.profileContent)
            try data?.write(to: baseURL.profileURL(name: $0.name))
        }
        print("saved provisioning profiles".green)
    }
    
    fileprivate func compare(profiles: [Attribute]) -> [Attribute] {
        var valuesToWrite: [Attribute] = []
        profiles.forEach {
            let profilePath = URL.provisioningProfilesPath().profileURL(name: $0.name)
            guard let data = try? Data(contentsOf: profilePath) else {
                valuesToWrite.append($0)
                print("New profile \($0.name)".green)
                return
            }
            let newProfileData = Data(base64Encoded: $0.profileContent)
            if data.sha256 != newProfileData?.sha256 {
                valuesToWrite.append($0)
                try? FileManager.default.removeItem(at: profilePath)
            } else {
                print("No change in profile \($0.name)".blue)
            }
        }
        return valuesToWrite
    }
}
