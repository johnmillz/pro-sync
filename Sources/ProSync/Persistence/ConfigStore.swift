//
//  ConfigStore.swift
//  ProSync
//
//  Created by John Mills on 31/03/2021.
//  Copyright © 2021 John Mills. All rights reserved.
//

import Foundation

protocol ConfigStoring {
    func saveTeamID(_ teamID: String) throws
}

protocol ConfigLoading {
    func loadTeamIDs() throws -> Set<String>
}

struct ConfigStore: ConfigStoring, ConfigLoading {

    let fileURL: URL

    init?() {
        guard let userDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.deletingLastPathComponent() else {
            return nil
        }
        fileURL = userDir.appendingPathComponent(".pro-sync")
    }

    func saveTeamID(_ teamID: String) throws {
        do {
            let teams = try String(contentsOf: fileURL)
            var components = teams.components(separatedBy: "\n").compactMap{ $0.isEmpty ? nil : $0 }
            guard !components.contains(teamID) else { return }
            components.append(teamID)
            let newConfig = components.joined(separator: "\n")
            try newConfig.write(to: fileURL, atomically: true, encoding: .utf8)
            print("saved team".green)
        } catch {
            try "\(teamID)\n".write(to: fileURL, atomically: true, encoding: .utf8)
            print("saved team".green)
        }
    }

    func loadTeamIDs() throws -> Set<String> {
        let teams = try String(contentsOf: fileURL)
        print("loaded teams".blue)
        let components = teams.components(separatedBy: "\n").compactMap{ $0.isEmpty ? nil : $0 }
        return Set(components)
    }
}
