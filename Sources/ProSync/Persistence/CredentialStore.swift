//
//  CredentialStore.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import KeychainAccess

struct Credential {
    let keyID, issuerID, privateKeyString: String
}

protocol CredentialStoring {
    func saveCredential(_ credential: Credential)
}

protocol CredentialLoading {
    func loadCredentials() throws -> Credential
}

struct CredentialStore: CredentialStoring, CredentialLoading {

    let teamID: String

    func saveCredential(_ credential: Credential) {
        let items = [KeychainAccount.key: credential.keyID,
                     KeychainAccount.issuer: credential.issuerID,
                     KeychainAccount.privateKey: credential.privateKeyString]
        
        let keychain = Keychain(service: "\(KeychainService.name)-\(teamID)")
        for (_, item) in items.enumerated() {
            keychain[item.key.stringValue] = item.value
        }
        print("saved credentials".green)
    }
    
    func loadCredentials() throws -> Credential {
        let keychain = Keychain(service: "\(KeychainService.name)-\(teamID)")
        guard let keyID = try keychain.get(KeychainAccount.key.stringValue),
            let issuerID = try keychain.get(KeychainAccount.issuer.stringValue),
            let privateKeyString = try keychain.get(KeychainAccount.privateKey.stringValue) else {
                throw KeychainError.itemNotFound
        }
        
        print("loaded credentials".blue)
        return Credential(keyID: keyID, issuerID: issuerID, privateKeyString: privateKeyString)
    }
}
