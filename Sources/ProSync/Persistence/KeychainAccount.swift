//
//  KeychainAccount.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

enum KeychainAccount: String, CodingKey {
    case key = "kid"
    case issuer = "iss"
    case privateKey = "pk"
}
