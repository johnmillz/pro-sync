//
//  TransientCredentialStore.swift
//  pro-sync
//
//  Created by John Mills on 23/06/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation

class TransientCredentialStore: CredentialStoring, CredentialLoading {

    var credential: Credential?

    func saveCredential(_ credential: Credential) {
        self.credential = credential
    }

    func loadCredentials() throws -> Credential {
        guard let credential = credential else { throw AppError.credentialNotFound }
        return credential
    }
}
