//
//  main.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import SwiftCLI

let cli = CLI(name: "pro-sync", version: "0.4.2", description: "CI/CD gateway to App Store Connect")
cli.commands = [Setup(), DownloadProfiles()]
cli.goAndExit()
