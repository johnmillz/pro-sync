//
//  MIMEType.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

/// "The MIME type is based on the information provided from an origin source".
/// Can be considered the content type of the response/request.
enum MIMEType: String {
    case json = "application/json"
    case jsonapi = "application/vnd.api+json"
}
