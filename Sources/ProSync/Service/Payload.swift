//
//  Payload.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import SwiftJWT

/// Acts as the "JWT Payload" as defined in app store connect API documentation.
/// This is later signed along with the header to assure the payload is marhalled by the key holder.
struct Payload: Claims {
    var iss: String
    var aud: String
    var exp: Date?
    init(issuer: String) {
        self.iss = issuer
        self.aud = "appstoreconnect-v1"
        exp = Date.expiryWindow
    }
}

fileprivate extension Date {
    /// Generates a date between 3 and 15 mins in the future.
    /// The upper limit of expiration allowed is 20 mins as documented:
    /// https://developer.apple.com/documentation/appstoreconnectapi/generating_tokens_for_api_requests
    static var expiryWindow: Date {
        let cal = Calendar.init(identifier: .gregorian)
        return cal.date(byAdding: .minute, value: 15, to: Date()) ?? Date().addingTimeInterval(180)
    }
}
