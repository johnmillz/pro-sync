//
//  HTTPSClient.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import PromiseKit
import Rainbow

/// Basic HTTPS client for making requests to the Apple domain.
class HTTPSClient: NSObject {
    
    var receivedData: Data?
    private let (promise, seal) = Promise<Data>.pending()
       
    func makeRequest(_ request: URLRequest) -> Promise<Data> {
        receivedData = Data()
        let session = URLSession(configuration: URLSessionConfiguration.ephemeral, delegate: self, delegateQueue: .main)
        let task = session.dataTask(with: request)
        task.resume()
        return promise
    }
}

extension HTTPSClient: URLSessionDelegate, URLSessionDataDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust,
            challenge.protectionSpace.host.hasSuffix("apple.com") else {
                completionHandler(.cancelAuthenticationChallenge, nil)
                return
        }

        completionHandler(.performDefaultHandling, nil)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        guard let response = response as? HTTPURLResponse else {
            completionHandler(.cancel)
            seal.reject(AppError.unknownError)
            return
        }
            
        guard (200...299).contains(response.statusCode) else {
            completionHandler(.cancel)
            seal.reject(RequestError(rawValue: response.statusCode) ?? AppError.unknownError)
            return
        }
        
        guard let mimeType = response.mimeType else {
            completionHandler(.cancel)
            seal.reject(AppError.missingMimeType)
            return
        }
        
        switch MIMEType(rawValue: mimeType) {
            case .json, .jsonapi: completionHandler(.allow)
            default: seal.reject(AppError.unknownMimeType)
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        print("receiving data..".blue)
        self.receivedData?.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard error == nil else {
            print("error receiving data".red.onBlack)
            seal.reject(error!)
            return
        }
        
        if let receivedData = self.receivedData {
            print("finished receiving data".blue)
            seal.fulfill(receivedData)
        }
    }
}
