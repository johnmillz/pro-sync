//
//  URL+Profiles.swift
//  pro-sync
//
//  Created by Mills John on 15/12/2021.
//  Copyright © 2021 John Mills. All rights reserved.
//

import Foundation

extension URL {
    static func provisioningProfilesPath() -> URL {
        let provisioningPathComponents = "MobileDevice/Provisioning Profiles/"
        let library = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        return library.appendingPathComponent(provisioningPathComponents)
    }
    
    func profileURL(name: String) -> URL {
        self.appendingPathComponent(name).appendingPathExtension("mobileprovision")
    }
}
