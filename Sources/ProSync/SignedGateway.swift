//
//  SignedGateway.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import SwiftJWT

protocol SignedGateway {
    func createRequest(endpoint: String, queryItems: [URLQueryItem], credentialStore: CredentialLoading) throws -> URLRequest
}

extension SignedGateway {
    fileprivate func generateSignedToken(credentialStore: CredentialLoading) throws -> String {
        let credential = try credentialStore.loadCredentials()
        
        guard let privateKeyData = credential.privateKeyString.data(using: .utf8) else {
            throw AppError.invalidSigningID
        }
        
        let signer = JWTSigner.es256(privateKey: privateKeyData)
        let header = Header(typ: "JWT", kid: credential.keyID)
        var jwt = JWT(header: header, claims: Payload(issuer: credential.issuerID))
        return try jwt.sign(using: signer)
    }
    
    func createRequest(endpoint: String, queryItems: [URLQueryItem], credentialStore: CredentialLoading) throws -> URLRequest {
        var url = URLComponents(string: "https://api.appstoreconnect.apple.com/v1/\(endpoint)")!
        url.queryItems = queryItems
        var request = URLRequest(url: url.url!)
        request.addValue("Bearer \(try generateSignedToken(credentialStore: credentialStore))", forHTTPHeaderField: "Authorization")
        return request
    }
}
