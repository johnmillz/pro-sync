//
//  KeychainError.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

enum KeychainError: Int32, Error {
    case itemNotFound = -25300 // errSecItemNotFound
    case duplicateItem = -25299 // errSecDuplicateItem
    case unknownError
    
    init(osStatus: Int32) {
        switch osStatus {
            case KeychainError.itemNotFound.rawValue: self = .itemNotFound
            default: self = .unknownError
        }
    }
}
