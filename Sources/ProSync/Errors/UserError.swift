//
//  UserError.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

/// Errors made by the user of this app, probably based on the input they have provided.
enum UserError: Error {
    case missingInput
    case invalidInput
}
