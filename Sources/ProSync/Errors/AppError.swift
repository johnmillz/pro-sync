//
//  AppError.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

/// Errors the app can generate owing to a lack of handling.
enum AppError: Error {
    case invalidSigningID
    case missingMimeType
    case unknownMimeType
    case unknownError
    case credentialNotFound
    case teamNotFound
}
