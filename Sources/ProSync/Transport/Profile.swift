//
//  Profile.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

struct Profile: Codable {
    var type: String
    var id: String
    var attributes: Attribute
}

struct ProfileResponse: Codable {
    var data: [Profile]
}
