//
//  Profiles.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

struct Attribute: Codable {
    var profileState: String
    var createdDate: String
    var profileType: String
    var name: String
    var profileContent: String
    var uuid: String
    var platform: String
    var expirationDate: String
}
