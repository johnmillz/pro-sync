//
//  Setup.swift
//  pro-sync
//
//  Created by John Mills on 02/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import KeychainAccess
import SwiftCLI

class Setup: Command {
    let name = "setup"
    let shortDescription = "Sets up the tool on this mac by importing keys for later authentication"

    let keyID = Key<String>("-k", "--key-id", description: "ASC Key ID")
    let issuerID = Key<String>("-i", "--issuer-id", description: "ASC Issuer ID")
    let privateKeyPath = Key<String>("-p", "--private-key-path", description: "ASC Private Key Path")
    let teamID = Key<String>("-t", "--team-id", description: "Team ID associated with the credentials")
    
    func execute() throws {
        let inputProcessor = CredentialInputProcessor()
        inputProcessor.keyID = keyID.value
        inputProcessor.issuerID = issuerID.value
        inputProcessor.privateKeyPath = privateKeyPath.value
        inputProcessor.teamID = teamID.value
        let inputs = try inputProcessor.extractInputs()
        CredentialStore(teamID: inputs.teamID).saveCredential(inputs.credential)
        guard let configStore = ConfigStore() else {
            throw AppError.unknownError
        }
        try configStore.saveTeamID(inputs.teamID)
    }
}
