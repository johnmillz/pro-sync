//
//  DownloadProfiles.swift
//  pro-sync
//
//  Created by John Mills on 01/05/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation
import SwiftCLI
import SwiftJWT
import PromiseKit

class DownloadProfiles: Command {
    
    let name: String = "download-profiles"
    let shortDescription = "Downloads all non xcode-managed profiles from App Store Connect"

    let keyID = Key<String>("-k", "--key-id", description: "ASC Key ID")
    let issuerID = Key<String>("-i", "--issuer-id", description: "ASC Issuer ID")
    let privateKeyPath = Key<String>("-p", "--private-key-path", description: "ASC Private Key Path")
    let teamID = Key<String>("-t", "--team-id", description: "Team ID associated with the credentials")

    let outputProfilesPath = Key<String>("-o", "--output-Profiles-Path", description: "ASC Private Key Path")
    
    @Flag("-d", "--diff", description: "Instead of removing existing profiles, only update new profiles")
    var diffMode: Bool

    fileprivate func saveCredentials(_ inputProcessor: CredentialInputProcessor) throws -> TransientCredentialStore {
        let inputs = try inputProcessor.extractInputs()
        let credentialStore = TransientCredentialStore()
        credentialStore.saveCredential(inputs.credential)
        return credentialStore
    }

    fileprivate func downloadProfiles(_ credentialStore: CredentialLoading, mode: AppStoreConnect.SaveMode) throws {
        let asc = AppStoreConnect(credentialStore: credentialStore)
        try asc.downloadProfiles(mode: mode)
    }

    func execute() throws {
        let credentialStore: CredentialLoading
        let inputProcessor = CredentialInputProcessor()
        inputProcessor.keyID = keyID.value
        inputProcessor.issuerID = issuerID.value
        inputProcessor.privateKeyPath = privateKeyPath.value
        inputProcessor.teamID = teamID.value

        let mode: AppStoreConnect.SaveMode = diffMode ? .diff : .sync
        
        if mode == .sync {
            self.clearProfilesDirectory()
        }
        
        #if os(Linux)
        credentialStore = try saveCredentials(inputProcessor)
        #else
        if keyID.value == nil, let configStore = ConfigStore() {
            let teams = try configStore.loadTeamIDs()
            for team in teams {
                let credentialStore = CredentialStore(teamID: team)
                try downloadProfiles(credentialStore, mode: mode)
            }

        } else {
            credentialStore = try saveCredentials(inputProcessor)
            try downloadProfiles(credentialStore, mode: mode)
        }
        #endif
    }
    
    fileprivate func clearProfilesDirectory() {
        try? FileManager.default.removeItem(at: URL.provisioningProfilesPath())
    }
}
