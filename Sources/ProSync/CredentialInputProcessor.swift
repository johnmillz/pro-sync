//
//  CredentialInputProcessor.swift
//  pro-sync
//
//  Created by John Mills on 23/06/2020.
//  Copyright © 2020 John Mills. All rights reserved.
//

import Foundation

protocol CredentialInputProcessing {
    func extractInputs() throws -> (credential: Credential, teamID: String)
}

class CredentialInputProcessor: CredentialInputProcessing {

    var keyID: String?
    var issuerID: String?
    var privateKeyPath: String?
    var teamID: String?

    func extractInputs() throws -> (credential: Credential, teamID: String) {
        guard let keyID = keyID else { throw UserError.missingInput }
        guard let issuerID = issuerID else { throw UserError.missingInput }
        guard let privateKeyPath = privateKeyPath else { throw UserError.missingInput }
        guard let teamID = teamID else { throw UserError.missingInput }

        let filePath = URL(fileURLWithPath: privateKeyPath, isDirectory: false)
        let privateKeyData = try Data(contentsOf: filePath)
        guard let privateKeyString = String(data: privateKeyData, encoding: .utf8) else { throw AppError.invalidSigningID }

        return (Credential(keyID: keyID, issuerID: issuerID, privateKeyString: privateKeyString), teamID)
    }
}
