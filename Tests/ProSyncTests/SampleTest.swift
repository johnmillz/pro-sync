import XCTest

final class SampleTest: XCTestCase {
    func testGivenNoInput_thenMissingInputErrorThrown() {
        print("HelloWorld")
    }

    static var allTests = [
        ("testGivenNoInput_thenMissingInputErrorThrown", testGivenNoInput_thenMissingInputErrorThrown),
    ]
}
